<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('inicio');
});

Route::get('/contacto', function () {
    return view('contacto');
});
Route::get('/Servicio-CheckingFixture', function () {
    return view('scf');
});
Route::get('/Servicio-HoldingFixture', function () {
    return view('shf');
});
Route::get('/Maquinado', function () {
    return view('maquinado');
});
Route::get('/CheckingFixture', function () {
    return view('refacciones');
});
Route::get('/HoldingFixture', function () {
    return view('moldes');
});
Route::get('/DispositivoDeGrabado', function () {
    return view('despieces');
});
Route::get('/Maquinado-Forming-En-Duro', function () {
    return view('formingenduro');
});
Route::get('/Maquinado-LiftingPlate', function () {
    return view('paginas/liftingplate');
});
Route::get('/Maquinado-PizadorInferiorDuro', function () {
    return view('paginas/pizador');
});
Route::get('/Maquinado-Proyecto', function () {
    return view('paginas/53-1');
});
Route::get('/Maquinado-MoldeParaInyeccionDePlastico', function () {
    return view('paginas/moldeinyeccion');
});
Route::get('/Maquinado-MoldeParaFundicion', function () {
    return view('paginas/moldefundicion');
});
Route::get('/servicios-rectificado', function () {
    return view('paginas/rectificado');
});
Route::get('/servicios-centro-de-maquinado-cnc', function () {
    return view('paginas/centrocnc');
});
Route::get('/servicios-maquinados-convencionales', function () {
    return view('paginas/convencionales');
});
/*
|--------------------------------------------------------------------------
| RUTAS DE PROYECTOS CHECKING FIXTURE
|--------------------------------------------------------------------------
*/
Route::get('/PROYECTOS-DC_963_PRENSA_GENERAL_PRESS_PART', function () {
    return view('cfixture/cf');
});
Route::get('/PROYECTOS-BMOK-60000462455TEM1000', function () {
    return view('cfixture/cf1');
});
Route::get('/PROYECTOS-5Q0 803 113 A_LH-RH', function () {
    return view('cfixture/cf2');
});
Route::get('/PROYECTOS-BM5U386588LH-386589RH', function () {
    return view('cfixture/cf3');
});
Route::get('/PROYECTOS-BMOK-60000462455TEM1000', function () {
    return view('cfixture/cf4');
});
Route::get('/PROYECTOS-VW518-858-965A', function () {
    return view('cfixture/cf5');
});
// Route::get('/PROYECTOS-613586_AH_T01_CLOSING_PLATE_RLCA', function () {
//     return view('cfixture/cf6');
// });
Route::get('/PROYECTOS-DR77-648164', function () {
    return view('cfixture/cf7');
});
// Route::get('/PROYECTOS-BM5U386588LH-386589RH', function () {
//     return view('cfixture/cf8');
// });
Route::get('/PROYECTOS-615296 RLCA-ASSY', function () {
    return view('cfixture/cf9');
});
Route::get('/PROYECTOS-613586_AH_T01_CLOSING_PLATE_RLCA', function () {
    return view('cfixture/cf10');
});
Route::get('/PROYECTOS-DC_615296_FORD_CX727', function () {
    return view('cfixture/cf11');
});
Route::get('/PROYECTOS-DR77497941', function () {
    return view('cfixture/cf12');
});
Route::get('/PROYECTOS-706317_AD_000_DEFOELEMENT_AUSSEN_LI_VW316_SA', function () {
    return view('cfixture/cf13');
});
Route::get('/PROYECTOS-VWM-BUMPER_TAREK_706316-318', function () {
    return view('cfixture/cf14');
});
Route::get('/PROYECTOS-DR77_60000422137TEM_ZBQUERTRAEGER', function () {
    return view('cfixture/cf15');
});
Route::get('/PROYECTOS-BM5U386608_92347', function () {
    return view('cfixture/cf16');
});
Route::get('/PROYECTOS-DR77-490122', function () {
    return view('cfixture/cf17');
});
Route::get('/PROYECTOS-CHECKING FIXTURE', function () {
    return view('cfixture/cf18');
});
Route::get('/PROYECTOS-534118_M09_Z01_GEM170-ASSY-TWISTBEAM', function () {
    return view('cfixture/cf19');
});
/*
|--------------------------------------------------------------------------
| RUTAS DE PROYECTOS HOLDING FIXTURE
|--------------------------------------------------------------------------
*/

Route::get('/PROYECTOS-702024-CMMFIXTURES', function () {
    return view('hfixture/hf');
});
Route::get('/PROYECTOS-701932-CMMFEXTURES', function () {
    return view('hfixture/cf1');
});
Route::get('/PROYECTOS-672645_CMMFIXTURES', function () {
    return view('hfixture/cf2');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

