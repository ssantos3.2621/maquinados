# pagina web maquinadosAndres

Pagina Informativa para una empresa en la industria metalMecanica Basado en Laravel 5.8

## Requirements

- Laravel 5.8
- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- BCMath PHP Extension

## instrucciones de instalacion

```
git clone https://gitlab.com/ssantos3.2621/maquinados.git
cd maquinados
=Abrir la terminal y ejecutar=
composer install
cp '.env.example' renombrar a '.env'
php artisan key:generate
=crear BD en mysql, colocar nombreBD NombreUser Y Password en el archivo .env=
php artisan migrate


## Author

- [Samuel Santos](https://www.animatiomx.com)
