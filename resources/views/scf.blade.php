@extends('layouts.header')
@section('content')
<!--header fijo--->
<div class="container-fluid p-0 m-0 padingtop">
    <div class="item">
      <img class="img-fluid full-width" src="public/images/SERVICIOS-SLIDER.jpg" alt="">
    </div>
   </div>
<!--fin header fijo--->

<!--seccion contenido Diseño-->
<div class="container text-center" id="imagenesweb">
  <div class="row">
  <div class="col-md-1"></div>
  <div class="col-md-10 text-center">
    <h2 style="padding-top: 5%" class="flash wow">CHECKING & HOLDING FIXTURE (HF’S)</h2>
    <br>
    <h6 class="text-center" style="color: #4d4d4d!important;font-weight: 200;font-size: 35px">¿Qué es?</h6><br>
    
    <h6 class="text-center flipInY wow" style="color: #4d4d4d!important;font-weight: 200;font-size: 30px">Diseño, manufactura y certificación de holding y checking fixtures aplicando sistemas CAD/CAM en nuestros procesos de diseño y producción, apegándonos a los requerimientos y estándares de cada uno de nuestros clientes. </h6><br>
  </div>
  <div class="col-md-1"></div>
    </div>

   <div class="col-lg-12 col-md-12 col-sm-12 " style="margin-top:55px">
         <div class="isotope columns-2 popup-gallery">
              <div class="grid-item photography branding">
                  <div class="portfolio-item">
                   <img src="public/images/otro-servicio/s1.jpg" alt="">
                    <a class="popup portfolio-img" href="public/images/otro-servicio/s1.jpg"><i class="fa fa-arrows-alt" style="font-size: 30px!important;margin-top:17px!important"></i></a>
                </div>
               </div>
              <div class="grid-item photography branding">
               <div class="portfolio-item">
                   <img src="public/images/otro-servicio/s2.jpg" alt="">
                    <a class="popup portfolio-img" href="public/images/otro-servicio/s2.jpg"><i class="fa fa-arrows-alt" style="font-size: 30px!important;margin-top:17px!important"></i></a>
                </div>
              </div>
              <div class="grid-item photography branding">
                  <div class="portfolio-item">
                   <img src="public/images/otro-servicio/s3.jpg" alt="">
                    <a class="popup portfolio-img" href="public/images/otro-servicio/s3.jpg"><i class="fa fa-arrows-alt" style="font-size: 30px!important;margin-top:17px!important"></i></a>
                </div>
               </div>
              <div class="grid-item photography branding">
               <div class="portfolio-item">
                   <img src="public/images/otro-servicio/cf.jpg" alt="">
                    <a class="popup portfolio-img" href="public/images/otro-servicio/cf.jpg"><i class="fa fa-arrows-alt" style="font-size: 30px!important;margin-top:17px!important"></i></a>
                </div>
              </div>
         </div>
            
       </div>

       <br><br>
<div class="row">
  <div class="col-md-1"></div>
    <div class="col-md-10"> <p  class="text-justify flipInY wow">Nuestros equipos y personal calificado nos permiten cumplir con las exigencias dimensionales de esta clase de dispositivos, proveyendo así, de herramientas confiables y funcionales a nuestros clientes.</p></div>
    <div class="col-md-1"></div>
</div>

</div>
<!--fin seccion contenido Diseño-->

  <div class="container" id="imagenescelular" style="margin-top: 30px">

    <h2 style="padding-top: 5%" class="flash wow text-center">CHECKING & HOLDING FIXTURE (HF’S)</h2>
    <br>
    <h6 class="text-center" style="color: #4d4d4d!important;font-weight: 200;font-size: 35px">¿Qué es?</h6><br>
    
    <p class="text-justify flipInY wow">Diseño, manufactura y certificación de holding y checking fixtures aplicando sistemas CAD/CAM en nuestros procesos de diseño y producción, apegándonos a los requerimientos y estándares de cada uno de nuestros clientes. </p><br>
   <div class="row">
     <div class="col-lg-6 sm-mb-30">
       <div class="owl-carousel popup-gallery" data-nav-dots="true" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-xx-items="1" data-space="20">
         <div class="item">
            <div class="portfolio-item">
                   <img src="public/images/otro-servicio/s1.jpg" alt="">
                    <a class="popup portfolio-img" href="public/images/otro-servicio/s1.jpg"><i class="fa fa-arrows-alt"></i></a>
            </div>
          </div>
          <div class="item">
            <div class="portfolio-item">
                   <img src="public/images/otro-servicio/s2.jpg" alt="">
                    <a class="popup portfolio-img" href="public/images/otro-servicio/s2.jpg"><i class="fa fa-arrows-alt"></i></a>
            </div>
          </div>
          <div class="item">
            <div class="portfolio-item">
                   <img src="public/images/otro-servicio/s3.jpg" alt="">
                    <a class="popup portfolio-img" href="public/images/otro-servicio/s3.jpg"><i class="fa fa-arrows-alt"></i></a>
            </div>
          </div>
          <div class="item">
            <div class="portfolio-item">
                   <img src="public/images/otro-servicio/cf.jpg" alt="">
                    <a class="popup portfolio-img" href="public/images/otro-servicio/cf.jpg"><i class="fa fa-arrows-alt"></i></a>
            </div>
          </div>
      </div>
     </div>
   </div>

   <p class="text-justify flipInY wow">Nuestros equipos y personal calificado nos permiten cumplir con las exigencias dimensionales de esta clase de dispositivos, proveyendo así, de herramientas confiables y funcionales a nuestros clientes.</p>
   </div>

<!---formulario de contacto y mapa-->
<div class="container text-center pt-5">
  <h1 class="rubberBand wow">CONTACTANOS</h1>
    <div class="row" style="padding-top:5%;padding-bottom:10%">

      <div class="col-lg-6 sm-mb-30" style="background: url(public/images/imagen_formulario.jpg);background-position: center;background-repeat: no-repeat;background-size: cover;position: relative;">
      </div>

        <div class="col-lg-6">
          <div id="formmessage">Success/Error Message Goes Here</div>
            <form id="contactform" role="form" method="post" action="php/contact-form.php">
              <div class="contact-form form-inline clearfix">
                <div class="section-field">
                  <input id="name" type="text" placeholder="Nombre*" class="form-control" name="name">
                </div>
                <div class="section-field">
                  <input type="email" placeholder="Email*" class="form-control" name="email">
                </div>
                <div class="section-field">
                  <input type="number" placeholder="Teléfono*" class="form-control" name="phone">
                </div>
        
                <div class="section-field xs-w-100" style="width: 36%">
                  <input type="text" placeholder="Asunto*" class="form-control" name="asunto">
                </div>
                
                <div class="section-field selectformulario1 xs-mb-20">
                  <input style="color: #84ba3f;font-weight: 900;margin-bottom: 5px;" disabled type="text" placeholder="CHECKING & HOLDING FIXTURE (HF’S)" class="form-control" name="producto" value="CHECKING & HOLDING FIXTURE (HF’S)">
                </div>

                <div class="section-field textarea">
                  <textarea class="input-message form-control" placeholder="Mensaje*" rows="7" name="message"></textarea>
                </div>
                <!-- Google reCaptch-->
              <div class="g-recaptcha section-field clearfix d-flex justify-content-center" data-sitekey="6LdwyNUUAAAAADhX6v7h8W7nARWWRpAUDbZknT1t"></div>
                <div class="form-control submit-button text-center" style="background-color: transparent;">
                  <input type="hidden" name="action" value="sendEmail">
                  <button id="submit" name="submit" type="submit" value="Send" class="button rounded-pill pt-1 pb-1"><span>ENVIAR</span></button>
                </div>
              </div>
            </form>
            <div id="ajaxloader" style="display:none"><img class="mx-auto mt-30 mb-30 d-block" src="public/images/loader-04.svg" alt=""></div>
        </div>
    </div>
  </div>



@endsection
