@extends('layouts.header')
@section('content')
<!--seccion contenido Diseño-->
 <div class="container text-center" style="padding-top:130px;background-image: url(public/images/engranes_fondo.jpg);background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;" id="imagenesweb1">
  <div class="container-fluid">
    <hr style="border: 1px solid">
  </div>

<div class="row" style="padding-top: 5%;">

           <div class="col-lg-6">
             <div class="slider-slick">
              <div class="slider slider-for detail-big-car-gallery">
                    <img class="img-fluid" src="public/images/cf6.jpg" alt="">
                    <!-- <img class="img-fluid" src="public/images/maquinado1.jpg" alt="">
                    <img class="img-fluid" src="public/images/maquinado1-1.jpg" alt="">
                    <img class="img-fluid" src="public/images/maquinado6.jpg" alt="">
                    <img class="img-fluid" src="public/images/maquinado4.jpg" alt="">
                    <img class="img-fluid" src="public/images/maquinado5.jpg" alt=""> -->
                </div>
                <div class="slider slider-nav">
                   <!--  <img class="img-fluid" src="public/images/maquinado1.jpg" alt=""> -->
                    <!-- <img class="img-fluid" src="public/images/maquinado1.jpg" alt="">
                    <img class="img-fluid" src="public/images/maquinado1-1.jpg" alt="">
                    <img class="img-fluid" src="public/images/maquinado6.jpg" alt="">
                    <img class="img-fluid" src="public/images/maquinado4.jpg" alt="">
                    <img class="img-fluid" src="public/images/maquinado5.jpg" alt=""> -->
                </div>
             </div>
           </div>

      <div class="col-lg-6 port-information">
         <div class="port-title sm-mt-40">
            <h1 class="mb-30 flash wow">DR77-648164</h1>
         </div>
          <div class="tags mb-30">
            <!-- <h6 class="text-justify rollIn wow" style="margin-bottom:7%">Fabricamos estos dispositivos de control dimensional (checking fixtures) para la inspección de partes con geometrías complejas. Estos sistemas pueden ser del tipo Pasa / No Pasa o por variables para la recolección de datos. Estos equipos se operan manualmente o de forma automática dependiendo de la cantidad de partes a inspeccionar.</h6> -->

          <section class="" style="background-color: transparent;">
            <div class="col-lg-6 text-left">
                   <h1>CARACTERÍSTICAS</h1>
                </div><br>
           <div class="container text-left">
              <div class="row">

                <div class="col-md-6 centrarlistado">
                  <ul class="list mb-30">
                      <li style="font-weight: 800"><i class="fas fa-circle" style="color:#8cc63f;"></i>Diseño óptimo</li><br>
                      <li style="font-weight: 800"><i class="fas fa-circle" style="color:#8cc63f;"></i>Precisión precisa</li><br>
                      <li style="font-weight: 800"><i class="fas fa-circle" style="color:#8cc63f;"></i>Vida duradera</li><br>
                      <li style="font-weight: 800"><i class="fas fa-circle" style="color:#8cc63f;"></i>Robusto</li><br>
                  </ul>
                </div>
                <div class="col-md-6 centrarlistado">
                  <ul class="list mb-30">
                       <li style="font-weight: 800"><i class="fas fa-circle" style="color:#8cc63f;"></i>Duradero</li><br>
                       <li style="font-weight: 800"><i class="fas fa-circle" style="color:#8cc63f;"></i>Libre de óxido</li><br>
                       <li style="font-weight: 800"><i class="fas fa-circle" style="color:#8cc63f;"></i>Bajo mantenimiento</li><br>
                       <li style="font-weight: 800"><i class="fas fa-circle" style="color:#8cc63f;"></i>Operación fácil</li><br>
                     </ul>
                </div>
                
                <div class="col-lg-12 text-center tada wow">
                   <h3 id="separarteintereza" style="font-family: 'Vollkorn', serif!important;color:#2e3192!important;margin-top:5%;margin-bottom:7%">¿Te interesa?</h3>
                   <button id="submit" name="submit" type="submit" value="Send" class="button rounded-pill pt-1 pb-1 paddingbotonphone botonescontacto" ><i class="fab fa-whatsapp fa-lg"></i></button>

                   <button style="background:#2e3192!important;border: 2px solid #2e3192!important;" id="submit" name="submit" type="submit" value="Send" class="button rounded-pill pt-1 pb-1 paddingbotonphone botonescontacto"><i class="fas fa-phone-alt fa-lg"></i></button>
                </div>
               

              </div>
             </div>
            </section>

          </div>

       </div>
      </div>
</div>


 <div class="container text-center" style="padding-top: 15%" id="imagenescelular1">
  

<div class="row">
      

      <div class="col-lg-6 port-information">
         <div class="port-title sm-mt-40">
            <h1 class="mb-30 flash wow">DR77-648164</h1>
         </div>

         <div class="col-lg-6">
             <div class="slider-slick">
              <div class="slider slider-for detail-big-car-gallery">
                    <img class="img-fluid" src="public/images/cf6.jpg" alt="">
                  
                </div>
               
             </div>
           </div>

          <div class="tags mb-30" style="margin-top:15%">
            <!-- <h6 class="text-justify rollIn wow" style="margin-bottom:7%">Fabricamos estos dispositivos de control dimensional (checking fixtures) para la inspección de partes con geometrías complejas. Estos sistemas pueden ser del tipo Pasa / No Pasa o por variables para la recolección de datos. Estos equipos se operan manualmente o de forma automática dependiendo de la cantidad de partes a inspeccionar.</h6>
 -->
          <section class="">
            <div class="col-lg-6 text-left">
                   <h1>CARACTERÍSTICAS</h1>
                </div><br>
           <div class="container text-left">
              <div class="row">

                <div class="col-md-6 centrarlistado">
                  <ul class="list mb-30">
                      <li style="font-weight: 800"><i class="fas fa-circle" style="color:#8cc63f;"></i>Diseño óptimo</li><br>
                      <li style="font-weight: 800"><i class="fas fa-circle" style="color:#8cc63f;"></i>Precisión precisa</li><br>
                      <li style="font-weight: 800"><i class="fas fa-circle" style="color:#8cc63f;"></i>Vida duradera</li><br>
                      <li style="font-weight: 800"><i class="fas fa-circle" style="color:#8cc63f;"></i>Robusto</li><br>
                  </ul>
                </div>
                <div class="col-md-6 centrarlistado">
                  <ul class="list mb-30">
                       <li style="font-weight: 800"><i class="fas fa-circle" style="color:#8cc63f;"></i>Duradero</li><br>
                       <li style="font-weight: 800"><i class="fas fa-circle" style="color:#8cc63f;"></i>Libre de óxido</li><br>
                       <li style="font-weight: 800"><i class="fas fa-circle" style="color:#8cc63f;"></i>Bajo mantenimiento</li><br>
                       <li style="font-weight: 800"><i class="fas fa-circle" style="color:#8cc63f;"></i>Operación fácil</li><br>
                     </ul>
                </div>
                
                <div class="col-lg-12 text-center tada wow">
                   <h3 id="separarteintereza" style="font-family: 'Vollkorn', serif!important;color:#2e3192!important;margin-top:5%;margin-bottom:7%">¿Te interesa?</h3>
                   <button id="submit" name="submit" type="submit" value="Send" class="button rounded-pill pt-1 pb-1 paddingbotonphone botonescontacto" ><i class="fab fa-whatsapp fa-lg"></i></button>

                   <button style="background:#2e3192!important;border: 2px solid #2e3192!important;" id="submit" name="submit" type="submit" value="Send" class="button rounded-pill pt-1 pb-1 paddingbotonphone botonescontacto"><i class="fas fa-phone-alt fa-lg"></i></button>
                </div>
               

              </div>
             </div>
            </section>

          </div>

       </div>
      </div>
</div>
<!--fin seccion contenido Diseño-->


<!---formulario de contacto y mapa-->
<div class="container text-center pt-5">
  <h1 class="rubberBand wow">CONTACTANOS</h1>
    <div class="row" style="padding-top:5%;padding-bottom:10%">

      <div class="col-lg-6 sm-mb-30" style="background: url(public/images/imagen_formulario.jpg);background-position: center;background-repeat: no-repeat;background-size: cover;position: relative;">
      </div>

        <div class="col-lg-6">
          <div id="formmessage">Success/Error Message Goes Here</div>
            <form id="contactform" role="form" method="post" action="public/php/contact-form.php">
              <div class="contact-form form-inline clearfix">
                <div class="section-field">
                  <input id="name" type="text" placeholder="Nombre*" class="form-control" name="name">
                </div>
                <div class="section-field">
                  <input type="email" placeholder="Email*" class="form-control" name="email">
                </div>
                <div class="section-field">
                  <input type="number" placeholder="Teléfono*" class="form-control" name="phone">
                </div>
        
                <div class="section-field" style="width: 36%">
                  <input type="text" placeholder="Asunto*" class="form-control" name="asunto">
                </div>
                
                <div class="section-field selectformulario1">
                  <input style="color: #84ba3f;font-weight: 900;margin-bottom: 5px;" disabled type="text" placeholder="DR77-648164" class="form-control" name="producto" value="DR77-648164">
                </div>

                <div class="section-field textarea">
                  <textarea class="input-message form-control" placeholder="Mensaje*" rows="7" name="message"></textarea>
                </div>
                <!-- Google reCaptch-->
              <div class="g-recaptcha section-field clearfix" data-sitekey="6LdwyNUUAAAAADhX6v7h8W7nARWWRpAUDbZknT1t"></div>
                <div class="form-control submit-button text-center" style="background-color: transparent;">
                  <input type="hidden" name="action" value="sendEmail">
                  <button id="submit" name="submit" type="submit" value="Send" class="button rounded-pill pt-1 pb-1"><span>ENVIAR</span></button>
                </div>
              </div>
            </form>
            <div id="ajaxloader" style="display:none"><img class="mx-auto mt-30 mb-30 d-block" src="public/images/loader-04.svg" alt=""></div>
        </div>
    </div>
  </div>
@endsection
