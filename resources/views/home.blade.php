@extends('layouts.header')

@section('content')
<div class="container">
    

    <div class="row justify-content-center" style="padding-top:200px;">
        <div class="col-md-11 text-center">
            <h1>SUBIR IMAGENES</h1>
        </div>
        <div class="col-md-1">
            <button class="btn btn-success mr-2" data-toggle="modal" data-target="#exampleModalCenter">Agregar</button>
        </div>
    </div>

    <div class="row" style="height:auto!important;padding-bottom:200px;">
                <table class="table">
                    <thead class="thead-dark">
                        <tr class="no-border">
                        <th style="border:none;">Maquinados Industriales Andres</th>
                        <th style="border:none;">descripcion</th>
                        <th style="border:none;">archivo</th>
                        </tr>
                    </thead>
                    <tbody id="tbodyProducto">

      </tbody>
                    </table>
    </div>
    
</div>
<!--MODAL1-->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">SUBIR UNA NUEVA IMAGEN</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form >
          <div class="form-group">
            <label for="exampleFormControlFile1">SELECCIONE LA IMAGEN</label>
            <input type="file" class="form-control-file" id="file">
          </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
        <button type="button" id="btnsubirimagen" class="btn btn-primary">GUARDAR LOS CAMBIOS</button>
      </div>
    </div>
  </div>
</div>
<!--END MODAL1-->

<!--MODAL2-->
<div class="modal fade" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">CAMBIAR ESTA IMAGEN</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
          <input style="display:none" type="text" id="codigo"/>

        <form>
          <div class="form-group">
            <label for="exampleFormControlFile1">SELECCIONE LA IMAGEN</label>
            <input type="file" class="form-control-file" id="exampleFormControlFile1">
          </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
        <button type="button" class="btn btn-primary" id="btnsubirimagen1">GUARDAR LOS CAMBIOS</button>
      </div>
    </div>
  </div>
</div>
<div id="DataResult"></div>
<!--END MODAL2-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script>

window.onload=function() {
      $.ajax({
            type: "POST",
            url: 'https://animatiomx.com/maquinados/public/php/publicacion.php',
            data: {'cliente':'maquinadosandres','caso':0},
            dataType: "JSON",
            success: function(data){
         console.log(data);
  var html='';
 
              if(data.length > 0){
                  $.each(data, function(i,item){
                      html += '<tr>';
                            html += '<td style="width: 33%"><div class="isotope popup-gallery"><div class="portfolio-item"><img src="'+item.nombreimagen+'" width="20%"></div></div></td>';
                            html += '<td style="width: 33%">'+item.descripcion+'</td>';
                            html += '<td style="width: 33%"><button class="btn btn-primary mr-2" data-toggle="modal" onclick="recibir('+item.id+')" data-target="#exampleModalCenter1">Editar</button><button onclick="eliminarfoto('+item.id+')" class="btn btn-danger">Eliminar</button></td></td>';
 
                            html += '</tr>';
                    });
              }
 
                if(html == '') html = '<tr><td colspan="6">No existen datos..</td></tr>'
 
                $("#tbodyProducto").html(html);
            },
            error: function(response){      
            }
        });
    }
    
    
    function recibir(numero)
{
  var valor = numero;
  document.getElementById("codigo").value=valor;        
    
} 

    var arr;

  $( document ).ready(function() {


    $('#btnsubirimagen').on('click', function(){
        var fd = new FormData();
        var files = $('#file')[0].files[0];
        var name = 'maquinados';
        fd.append('file',files);
        
        if (files == '' || files == null){
            editar_sinimagen();
        }else{
            $.ajax({
            url: 'https://animatiomx.com/maquinados/public/uploadlaravel.php',
            type: 'post',
            data: fd,name,
            contentType: false,
            processData: false,
            success: function(response){
                if(response != 0){
                 console.log(response);
                 crear_publicacion(response);
                }else{
                    console.log('file not uploaded');
                }
            },
        });
        }

        
    });
    
     $('#btnsubirimagen1').on('click', function(){
        var fd = new FormData();
        var files = $('#exampleFormControlFile1')[0].files[0];
        var name = 'maquinados';
        fd.append('file',files);
        
        if (files == '' || files == null){
            editar_sinimagen();
        }else{
            $.ajax({
            url: 'https://animatiomx.com/maquinados/public/uploadlaravel.php',
            type: 'post',
            data: fd,name,
            contentType: false,
            processData: false,
            success: function(response){
                if(response != 0){
                 console.log(response);
                 editar_publicacion(response);
                }else{
                    console.log('file not uploaded');
                }
            },
        });
        }

        
    });
    
});

function crear_publicacion(res){

    var imagen ="http://animatiomx.com/maquinados/public/"+res; 
  $.ajax({
            type: "POST",
            url: 'https://animatiomx.com/maquinados/public/php/publicacion.php',
            data: {'imagen':imagen,'cliente':'maquinadosandres','caso':1},
            success: function(response){
               Swal.fire({
                position: "top-end",
                icon: "success",
                title: "Foto agregada correctamente",
                showConfirmButton: false,
                timer: 1500
              })

              setTimeout((function() {
                window.location.reload();
              }), 1500);
            },
            error: function(response){                
            }
        });
}

function editar_publicacion(res){
    
    var editarimagen=document.getElementById("codigo").value;

    var imagen ="http://animatiomx.com/maquinados/public/"+res; 
  $.ajax({
            type: "POST",
            url: 'https://animatiomx.com/maquinados/public/php/publicacion.php',
            data: {'imagen':imagen,'cliente':'maquinadosandres','caso':2,'idimagen':editarimagen},
            success: function(response){
               Swal.fire({
                position: "top-end",
                icon: "success",
                title: "Foto editada correctamente",
                showConfirmButton: false,
                timer: 1500
              })

              setTimeout((function() {
                window.location.reload();
              }), 1500);
            },
            error: function(response){                
            }
        });
}

function eliminarfoto(idfoto){
    var foto = idfoto;
  $.ajax({
            type: "POST",
            url: 'https://animatiomx.com/maquinados/public/php/publicacion.php',
            data: {'idimagen':foto,'caso': 3},
            success: function(response){
                Swal.fire({
                position: "top-end",
                icon: "success",
                title: "Foto eliminada correctamente",
                showConfirmButton: false,
                timer: 1500
              })

              setTimeout((function() {
                window.location.reload();
              }), 1500);
            },
            error: function(response){        
            }
        });
}

</script>
@endsection
