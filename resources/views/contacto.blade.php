@extends('layouts.header')
<!--header fijo--->
<div class="container-fluid p-0 m-0 padingtop">
                 <div class="item">
                  <img class="img-fluid full-width" src="public/images/CONTACTANOS-SLIDER.jpg" alt="">
                </div>
   </div>
<!--fin header fijo--->
@section('content')
<!--seccion de contacto-->
<section class="service white-bg">
  <div class="container text-center py-5">
  	<h2 class="slideInLeft wow">PONTE EN CONTACTO CON NOSOTROS</h2>
    <br><br>
    <div class="row pt-5">
         <div class="col-lg-4 col-md-4 pr-5 text-center  d-flex">
         	<img src="public/images/ubicacion.png" class="pb-3 zoomIn wow imagencontacto margenesimagenescontacto">
            <div class="mb-30 slideInLeft wow">
              <h3 style="text-align: left;">Ubicación</h3>
              <p class="datosempresa" style="text-align: left;">9 Oriente # 5 Col. Centro San Andres Cholula, Pue. C.P. 72810</p>
            </div>
         </div>
         <div class="col-lg-4 col-md-4 pr-5 text-center  d-flex">
         	<img src="public/images/telefono.png" class="pb-3 zoomIn wow imagencontacto margenesimagenescontacto">
            <div class="mb-30 slideInLeft wow">
              <h3 style="text-align: left;">Teléfono</h3>
              <p class="datosempresa" style="text-align: left;">22 22 47 59 76<br>22 2310 69 77</p>
            </div>
         </div>
         <div class="col-lg-4 col-md-4 pr-5 text-center  d-flex">
         	<img src="public/images/email.png" class="pb-3 imagencontacto zoomIn wow margenesimagenescontacto">
            <div class="mb-30 slideInLeft wow">
              
              <h3 style="text-align: left;">Email</h3>
              
              <p class="datosempresa" style="text-align: left;"><a style="color: #4d4d4d!important;" href="mailto:tmec.ind_andres@hotmail.com?Subject=Solicito%20Cotización" target="_top">tmec.ind_andres@hotmail.com</a></p>
              
            </div>
         </div>
      </div>
   </div>
 </section>
 <!--fin seccion de contacto-->

<!---formulario de contacto y mapa-->
<div class="container text-center pt-5">

    <div class="row" style="padding-bottom:10%">

          <div class="col-lg-6 col-md-6 col-sm-6 mb-40">
           <!--  <iframe src="https://maps.google.com/maps?width=100%&height=600&hl=es&coord=19.0467435,-98.3024265&q=Maquinados%20Industriales%20Andres%2C%20Centro%2C%20San%20Andr%C3%A9s%20Cholula%2C%20Puebla+(Maquinados%20Industriales%20Andres)&ie=UTF8&t=&z=20&iwloc=A&output=embed" class="mapatam"></iframe>
 -->
            <iframe src="https://www.google.com/maps/d/embed?mid=1Oo0SHWLYql4EjfDiuf7ZBQYD-j3uXg4z&hl=es&coord=19.0467435,-98.3024265&q=Maquinados%20Industriales%20Andres%2C%20Centro%2C%20San%20Andr%C3%A9s%20Cholula%2C%20Puebla+(Maquinados%20Industriales%20Andres)" 
            width="100" height="600" class="mapatam"></iframe>
        </div>

      <div class="col-lg-6">
        <h2 class="text-center slideInLeft wow">CONTACTANOS</h2>
        <h3 class="text-center infocontacto rubberBand wow">Pide informes o cotizaciones</h3>
        <br><br>
          <div id="formmessage">Success/Error Message Goes Here</div>
            <form id="contactform" role="form" method="post" action="php/contact-form.php">
              <div class="contact-form form-inline clearfix">
                <div class="section-field">
                  <input id="name" type="text" placeholder="Nombre*" class="form-control" name="name">
                </div>
                <div class="section-field">
                  <input type="email" placeholder="Email*" class="form-control" name="email">
                </div>
                <div class="section-field">
                  <input type="number" placeholder="Teléfono*" class="form-control" name="phone">
                </div>
        
                <div class="section-field">
                  <input type="text" placeholder="Asunto*" class="form-control" name="asunto">
                </div>
                <div class="section-field selectformulario1 xs-mb-20">
                  <div class="box">
                    <select class="wide fancyselect" name="producto">
                  
                      <option value="SIN SELECCIÓN" disabled="true" selected="true">Seleciona un Proyecto/servicio</option>
                      <option disabled="true">Seleciona un Proyecto</option>
                      <option value="FORMING EN DURO">FORMING EN DURO</option>
                      <option value="LIFTING PLATE">LIFTING PLATE</option>
                      <option value="PIZADOR INFERIOREN DURO">PIZADOR INFERIOREN DURO</option>
                      <option value="53-1_01_001_ZAPATA_INF_810_697_8">53-1_01_001_ZAPATA_INF_810_697_8</option>
                      <option value="MOLDE PARA INYECCION DE PLASTICO">MOLDE PARA INYECCION DE PLASTICO</option>
                      <option value="MOLDE PARA FUNDICION">MOLDE PARA FUNDICION</option>
                      <option value="AUQ5-389445M15000_SGR_HINTERBODEN">AUQ5-389445M15000_SGR_HINTERBODEN</option>
              <option value="VW37-526065M14X00_AH_SGR_MODULTRAEGER">VW37-526065M14X00_AH_SGR_MODULTRAEGER</option>
                      <option value="DC_963 PRENSA _GENERAL_PRESS PART">DC_963 PRENSA _GENERAL_PRESS PART</option>
                      <option value="5Q0 803 113 A_LH-RH">5Q0 803 113 A_LH-RH</option>
                      <option value="BMOK-60000462455TEM1000">BMOK-60000462455TEM1000</option>
                      <option value="VW518-858-965A">VW518-858-965A</option>
                      <option value="BM5U386588LH/386589RH">BM5U386588LH/386589RH</option>
                      <option value="DR77-648164">DR77-648164</option>
                      <option value="613586_AH_T01_CLOSING_PLATE_RLCA">613586_AH_T01_CLOSING_PLATE_RLCA</option>
                      <option value="DC_615296_FORD_CX727">DC_615296_FORD_CX727</option>
                      <option value="615296 RLCA-ASSY">615296 RLCA-ASSY</option>
                      <option value="706317_AD_000_DEFOELEMENT_AUSSEN_LI_VW316_SA">706317_AD_000_DEFOELEMENT_AUSSEN_LI_VW316_SA</option>
                      <option value="VWM-BUMPER_TAREK_706316/318">VWM-BUMPER_TAREK_706316/318</option>
                      <option value="DR77497941">DR77497941</option>
                      <option value="BM5U386608_92347">BM5U386608_92347</option>
                      <option value="DR77-490122">DR77-490122</option>
                      <option value="DR77_60000422137TEM_ZBQUERTRAEGER">DR77_60000422137TEM_ZBQUERTRAEGER</option>
                      <option value="534118_M09_Z01_GEM170-ASSY-TWISTBEAM">534118_M09_Z01_GEM170-ASSY-TWISTBEAM</option>
                      <option value="CHECKING FIXTURE">CHECKING FIXTURE</option>
                      <option value="VW37-526065M14X00_AH_SGR_MODULTRAEGER">VW37-526065M14X00_AH_SGR_MODULTRAEGER</option>

                      <option disabled="true">Seleciona un servicio</option>
                      <option value="CHECKING & HOLDING FIXTURE (HF’S)">CHECKING & HOLDING FIXTURE (HF’S)</option>
                      <option value="RECTIFICADO">RECTIFICADO</option>
                      <option value="CENTRO DE MAQUINADO CNC">CENTRO DE MAQUINADO CNC</option>
                      <option value="MAQUINADOS CONVENCIONALES">MAQUINADOS CONVENCIONALES</option>
                    </select>
                    
                  </div>
                </div>

                <div class="section-field textarea">
                  <textarea class="input-message form-control" placeholder="Mensaje*" rows="7" name="message"></textarea>
                </div>
                <!-- Google reCaptch-->
              <div class="g-recaptcha section-field clearfix d-flex justify-content-center" data-sitekey="6LdwyNUUAAAAADhX6v7h8W7nARWWRpAUDbZknT1t"></div>
                <div class="form-control submit-button text-center" style="background-color: transparent;">
                  <input type="hidden" name="action" value="sendEmail">
                  <button id="submit" name="submit" type="submit" value="Send" class="button rounded-pill pt-1 pb-1"><span>ENVIAR</span></button>
                </div>
              </div>
            </form>
            <div id="ajaxloader" style="display:none"><img class="mx-auto mt-30 mb-30 d-block" src="public/images/loader-04.svg" alt=""></div>
        </div>
    </div>
  </div>
 <!---fin formulario de contacto y mapa-->
@endsection
