@extends('layouts.header')
@section('content')
<!--header fijo--->
<div class="container-fluid p-0 m-0 padingtop">
    <div class="item">
      <img class="img-fluid full-width " src="public/images/PROYECTOS-SLIDER.jpg" alt="">
    </div>
   </div>
<!--fin header fijo--->

<!--seccion contenido Diseño-->
 <div class="container text-center">
  
    <h2 style="padding-top: 7%" class="flipInY wow">HOLDING FIXTURE</h2>

<div class="isotope columns-3 popup-gallery" style="position: relative; height: 1572.1px;padding-top:4%">
             
              <div class="grid-item photography branding" style="position: absolute; left: 0px; top: 0px;">
                  <div class="portfolio-item">
                   <img src="public/images/hf.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo" href="Servicio-HoldingFixture"> VW37-526065M14X00_AH_SGR_MODULTRAEGER  <br> Cliente: BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/hf.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
               </div>

         </div>

    </div>

    <br><br><br>

<!--fin seccion contenido Diseño-->


<!---formulario de contacto y mapa-->
<!-- <div class="container text-center pt-5">
  <h1 class="rubberBand wow">PIDE TU COTIZACIÓN</h1>
    <div class="row" style="padding-top:5%;padding-bottom:10%">

      <div class="col-lg-6 sm-mb-30" style="background: url(images/imagen_formulario.jpg);background-position: center;background-repeat: no-repeat;background-size: cover;position: relative;">
      </div>

        <div class="col-lg-6">
          <div id="formmessage">Success/Error Message Goes Here</div>
            <form id="contactform" role="form" method="post" action="php/contact-form.php">
              <div class="contact-form form-inline clearfix">
                <div class="section-field">
                  <input id="name" type="text" placeholder="Nombre*" class="form-control" name="name">
                </div>
                <div class="section-field">
                  <input type="email" placeholder="Email*" class="form-control" name="email">
                </div>
                <div class="section-field">
                  <input type="number" placeholder="Teléfono*" class="form-control" name="phone">
                </div>
        
                <div class="section-field" style="width: 36%">
                  <input type="text" placeholder="Asunto*" class="form-control" name="asunto">
                </div>
                <div class="section-field selectformulario">
                  <div class="box">
                    <select class="wide fancyselect" name="producto">
                      <option  disabled="true" selected="true">Maquinado</option>
                      <option value="FORMING EN DURO">FORMING EN DURO</option>
                      <option value="LIFTING PLATE">LIFTING PLATE</option>
                      <option value="PIZADOR INFERIOREN DURO">PIZADOR INFERIOREN DURO</option>
                      <option value="53-1 _ 01_001_ZAPATA_INF_810_697_8">53-1 _ 01_001_ZAPATA_INF_810_697_8</option>
                      <option value="MOLDE PARA INYECCION DE PLASTICO">MOLDE PARA INYECCION DE PLASTICO</option>
                      <option value="MOLDE PARA FUNDICION">MOLDE PARA FUNDICION</option>
                    </select>
                  </div>
                </div>

                <div class="section-field textarea">
                  <textarea class="input-message form-control" placeholder="Mensaje*" rows="7" name="message"></textarea>
                </div>

              <div class="g-recaptcha section-field clearfix" data-sitekey="6LdwyNUUAAAAADhX6v7h8W7nARWWRpAUDbZknT1t"></div>
                <div class="form-control submit-button text-center" style="background-color: transparent;">
                  <input type="hidden" name="action" value="sendEmail">
                  <button id="submit" name="submit" type="submit" value="Send" class="button rounded-pill pt-1 pb-1"><span>ENVIAR</span></button>
                </div>
              </div>
            </form>
            <div id="ajaxloader" style="display:none"><img class="mx-auto mt-30 mb-30 d-block" src="images/loader-04.svg" alt=""></div>
        </div>
    </div>
  </div> -->
@endsection
