
  <!--=================================
   mega menu -->

  <div class="menu fixed-top">
    <!-- menu start -->
     <nav id="menu" class="mega-menu">
      <!-- menu list items container -->
      <section class="menu-list-items">
       <div class="container">
        <div class="row">
         <div class="col-lg-12 col-md-12">
          <!-- menu logo -->
          <ul class="menu-logo" style="padding-right: 25px;">
              <li>
                  <a href="{{url('/')}}"><img id="logo_img" src="public/images/logo1.jpg" alt="logo"></a>

                  <a id="wts" style="display: none;padding-right:13%" class="float-right" href="tel:522222475976">
                    <img id="logo_img" src="public/images/llamada.png" style="height: 35px!important" alt="logo"> 
                  </a>
                  <a id="tel" style="display: none;padding-right:6%" class="float-right" href="https://wa.me/2223125949?text=Me%20gustaría%20pedir%20una%20cotización">
                    <img id="logo_img" src="public/images/whats_logo.png" style="height: 35px!important" alt="logo"> 
                  </a>

              </li>
          </ul>
          <!-- menu links -->
          <div class="menu-bar">
           <ul class="menu-links" style="font-family: 'Vollkorn', serif!important;">
             @guest
             <li><a href="{{url('/')}}">INICIO</a></li>
             <li><a href="javascript:void(0)"> SERVICIOS <i class="fa fa-angle-down fa-indicator"></i></a>
                   <!-- drop down multilevel  -->
                  <ul class="drop-down-multilevel">
                      <li><a href="Servicio-CheckingFixture">CHECKING & HOLDING FIXTURE (HF’S)</a></li>
                      <!-- <li><a href="Servicio-HoldingFixture"> FIXTURES (HF’S)</a></li> -->
                      <li><a href="servicios-rectificado">RECTIFICADO</a></li>
                      <li><a href="servicios-centro-de-maquinado-cnc">CENTRO DE MAQUINADO CNC</a></li>
                      <li><a href="servicios-maquinados-convencionales">MAQUINADOS CONVENCIONALES</a></li>
                  </ul>
              </li>
              <li><a href="javascript:void(0)">Proyectos<i class="fa fa-angle-down fa-indicator"></i></a>
                  <!-- drop down multilevel  -->
                  <ul class="drop-down-multilevel">
                      <li><a href="Maquinado">MAQUINADO</a></li>
                      <li><a href="CheckingFixture">CHECKING FIXTURE</a></li>
                      <li><a href="HoldingFixture">HOLDING FIXTURE</a></li>
                      <!-- <li><a href="DispositivoDeGrabado">DISPOSITIVO DE GRABADO</a></li> -->
                  </ul>
              </li>
              <li><a href="contacto"> CONTACTO </a></li>
              <li id="whats2"><a href="https://wa.me/2223125949?text=Me%20gustaría%20pedir%20una%20cotización"><img src="public/images/whats_logo.png" width="30%"></a></li>
              @else
              <li><a href="{{url('/')}}">INICIO</a></li>
             <li><a href="javascript:void(0)"> SERVICIOS <i class="fa fa-angle-down fa-indicator"></i></a>
                   <!-- drop down multilevel  -->
                  <ul class="drop-down-multilevel">
                      <li><a href="Servicio-CheckingFixture">CHECKING & HOLDING FIXTURE (HF’S)</a></li>
                      <!-- <li><a href="Servicio-HoldingFixture"> FIXTURES (HF’S)</a></li> -->
                      <li><a href="servicios-rectificado">RECTIFICADO</a></li>
                      <li><a href="servicios-centro-de-maquinado-cnc">CENTRO DE MAQUINADO CNC</a></li>
                      <li><a href="servicios-maquinados-convencionales">MAQUINADOS CONVENCIONALES</a></li>
                  </ul>
              </li>
              <li><a href="javascript:void(0)">Proyectos<i class="fa fa-angle-down fa-indicator"></i></a>
                  <!-- drop down multilevel  -->
                  <ul class="drop-down-multilevel">
                      <li><a href="Maquinado">MAQUINADO</a></li>
                      <li><a href="CheckingFixture">CHECKING FIXTURE</a></li>
                      <li><a href="HoldingFixture">HOLDING FIXTURE</a></li>
                      <!-- <li><a href="DispositivoDeGrabado">DISPOSITIVO DE GRABADO</a></li> -->
                  </ul>
              </li>
              <li><a href="contacto"> CONTACTO </a></li>
              <li><a  href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Cerrar Sesión') }}</a>
         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                   @csrf
         </form></li>
              <li id="whats2"><a href="https://wa.me/2223125949?text=Me%20gustaría%20pedir%20una%20cotización"><img src="public/images/whats_logo.png" width="30%"></a></li>
              @endguest
          </ul>
          </div>
         </div>
        </div>
       </div>
      </section>
     </nav>
    <!-- menu end -->
   </div>

