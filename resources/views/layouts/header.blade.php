<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="HTML5 Template" />
<meta name="description" content="Webster - Responsive Multi-purpose HTML5 Template" />
<meta name="author" content="potenzaglobalsolutions.com" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>MAQUINADOS INDUSTRIALES ANDRES</title>

<!-- Favicon -->
<link rel="shortcut icon" href="public/images/icon.png" />

<!-- font -->
<link href="https://fonts.googleapis.com/css?family=Vollkorn:400,600,700,900&display=swap" rel="stylesheet">

<link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,500,500i,600,700,800,900|Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700,800">

<!-- Plugins -->
  <!-- Plugins -->
  <link rel="stylesheet" type="text/css" href="{{url('public/css/plugins-css.css')}}" />

  <!-- revoluation -->
  <link rel="stylesheet" type="text/css" href="{{url('public/revolution/css/settings.css')}}" media="screen" />

  <!-- Typography -->
  <link rel="stylesheet" type="text/css" href="{{url('public/css/typography.css')}}" />

  <!-- Shortcodes -->
  <link rel="stylesheet" type="text/css" href="{{url('public/css/shortcodes/shortcodes.css')}}" />

  <!-- Style -->
  <link rel="stylesheet" type="text/css" href="{{url('public/css/style.css')}}" />

  <!-- Responsive -->
  <link rel="stylesheet" type="text/css" href="{{url('public/css/responsive.css')}}" />

  <!-- Style Custom -->
  <link rel="stylesheet" type="text/css" href="{{url('public/css/custom.css')}}" />





</head>

<body>

<div class="wrapper">

<!--=================================
 preloader -->

<div id="pre-loader">
    <img src="public/images/loader-04.svg" alt="">
</div>

<!--=================================
 preloader -->

 <!--=================================
 header -->

<header id="header" class="header light">

<!--=================================
 mega menu -->

 @include('layouts.menu')

</header>

<!--=================================
 header -->

		@yield('content')

		@extends('layouts.footer')
</div>



<div id="back-to-top"><a class="top arrow" href="#top"><i class="fa fa-angle-up"></i> <span>TOP</span></a></div>

<!--=================================
 jquery -->

<!-- font awesome -->
<script src="https://kit.fontawesome.com/5b24347517.js" crossorigin="anonymous"></script>
<!-- jquery -->
<script src="public/js/jquery-3.4.1.min.js"></script>

<!-- plugins-jquery -->
<script src="public/js/plugins-jquery.js"></script>

<!-- plugin_path -->
<script>var plugin_path = 'public/js/';</script>

<!-- custom -->
<script src="public/js/custom.js"></script>

<!-- REVOLUTION JS FILES -->
<script src="public/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="public/revolution/js/jquery.themepunch.revolution.min.js"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="public/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="public/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="public/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="public/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="public/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script src="public/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="public/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="public/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="public/revolution/js/extensions/revolution.extension.video.min.js"></script>

<!-- revolution custom -->
<script src="public/revolution/js/revolution-custom.js"></script>

<script src='https://www.google.com/recaptcha/api.js'></script>



</body>
</html>

