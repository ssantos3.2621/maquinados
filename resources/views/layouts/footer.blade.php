
  <!--=================================
 footer -->

<footer class="footer" style="background: url(public/images/footer.jpg);background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;">
 <div class="container">
  <div class="row">

      <div class="col-lg-3 col-sm-6 sm-mb-30 fadeInUpBig wow xs-mt-10 xs-mb-10" style="margin-top: 55px">
      <div class="footer-useful-link footer-hedding imagenresposiva">
        <a href="{{url('/')}}"><img id="imagenfooter" src="public/images/logo-footer.png" width="100%"> </a>
      </div>
    </div>

    <div id="responsivoempresa" class="col-lg-3 col-sm-6 sm-mb-30 fadeInUpBig wow" style="margin-top: 25px">
      <div class="footer-useful-link footer-hedding">
        <h1 style="color:#8cc63f!important;font-weight: bold;">EMPRESA</h1>
        <ul class="addresss-info" style="margin-top:5px">
          <li><i class="fas fa-circle" style="color:#8cc63f;font-size: 8px;margin-right:-10px"></i><a href="{{url('/')}}" class="letrafooter" style="font-weight: 600;">Inicio</a></li>
          <li><i class="fas fa-circle" style="color:#8cc63f;font-size: 8px;margin-right:-10px"></i><a href="#" class="letrafooter" style="font-weight: 600;">Productos</a></li>
          <li><i class="fas fa-circle" style="color:#8cc63f;font-size: 8px;margin-right:-10px"></i><a href="#" class="letrafooter" style="font-weight: 600;">Servicios</a></li>
          <li><i class="fas fa-circle" style="color:#8cc63f;font-size: 8px;margin-right:-10px"></i><a href="contacto" class="letrafooter" style="font-weight: 600;">Contacto</a></li>
        </ul>
      </div>
    </div>

    <div class="col-lg-3 col-sm-6 xs-mb-10 fadeInUpBig wow xs-mt-10 " style="margin-top: 25px">
    <h1 style="color:#8cc63f!important;font-weight: bold;" >CONTACTO</h1>
    <ul class="addresss-info float-right">
        <li><img src="public/images/ubicacion-01.png" width="10%">&nbsp;&nbsp;<span class="letrafooterb" style="font-weight: 600;margin-left:10px">9 Oriente # 5 Col. Centro San<span style="margin-left:45px"> Andrés Cholula, Pue. C.P. 72810</span></span></li>
        <li><img src="public/images/phone.png" width="10%">&nbsp;&nbsp;<span class="letrafooterb" style="font-weight: 600;margin-left:10px">22 22 47 59 76 / 22 2310 69 77</span></li>
        <li><img src="public/images/mail.png" width="10%">&nbsp;&nbsp;<a href="mailto:tmec.ind_andres@hotmail.com?Subject=Solicito%20Cotización" target="_top" class="letrafooterb" style="font-weight: 600;margin-left:10px">tmec.ind_andres@hotmail.com</a></li>
      </ul>
    </div>

    <div class="desaparece col-lg-3 col-sm-6 fadeInUpBig wow xs-mt-0" style="margin-top: 25px">

        <div class="row">
          <h1 id="redesfooter" style="color:#8cc63f!important;font-weight: bold;padding-right: 10px">ADMIN</h1>
          <div id="redesociales" class="footer-Newsletter d-flex justify-content-center">
            <!-- <div class="col-md-3"><img src="images/redes-01.png" width="100%"></div>
            <div class="col-md-3" style="margin-left: -8px"><img src="images/redes-03.png" width="100%"></div>
            <div class="col-md-3" style="margin-left: -11px"><img src="images/redes-02.png" width="100%"></div> -->
            <div class="col-md-12"><a class="btn btn-primary" href="login">INICIAR SESION</a></div>           
          </div>
        </div>
        <!-- <div class="row pt-3 pl-1 xs-pt-0">
          
        </div> -->
    </div>
        
       </div>

       <style type="text/css">
         .footer-widget{
          border-top: 5px solid #beb8bb;
         }
       </style>

      <div class="footer-widget xs-mt-0 xs-pt-10 xs-pb-0 mt-20 lineafooter">
        <div class="row">

          <div class="col-12 text-center" id="imagenesweb">
            <span class="letrafooterb"> 2020 Todos los derechos reservados </span><a href="" style="color: #f3cc23;; font-size: 1.3rem"><strong>ANIMATIOMX</strong></a>
          </div>

        </div>
      </div>

      <div class="footer-widget mt-20 lineafooter xs-pt-10 xs-pb-0" id="imagenescelular" style="margin-top: -35px!important">
        <div class="row">

          <div class="col-12" style="text-align: right;">
            <span class="letrafooterb"> 2020 Todos los derechos reservados </span>
          </div>

          <div class="col-12" style="text-align: right;">
            <a href="" style="color: #f3cc23;; font-size: 1.3rem"><strong>ANIMATIOMX</strong></a>
          </div>

        </div>
      </div>

  </div>
</footer>

<!--=================================
 footer -->