@extends('layouts.header')
@include('layouts.baner')
@section('content')
<style>

input[type=number]::-webkit-outer-spin-button,

input[type=number]::-webkit-inner-spin-button {

    -webkit-appearance: none;

    margin: 0;

}

 

input[type=number] {

    -moz-appearance:textfield;

}

</style>

<section class="service white-bg page-section-ptb">
  <div class="container">
    <!-- ============================================ -->
   <div class="service-3">
    <div class="row">

       <div class="col-lg-6 col-md-6 ">
          <div class="service-blog text-left">
             <h1 class="theme-color jackInTheBox wow">NOSOTROS</h1>
              <p class="text-justify jackInTheBox wow">En 2003 iniciamos operaciones en el ramo industrial. Desde entonces hemos crecido
				y consolidado una empresa de soluciones y procesos industriales con amplia experiencia en la industria metal-mecánica.</p>
              <!-- <b>01</b>
              <ul class="list list-unstyled">
                <li>Web Template(interface) design </li>
                <li>Static website design</li>
                <li>Custom web design</li>
              </ul> -->
          </div>
       </div>

       <div class="col-lg-6 col-md-6 xs-mt-30 xs-mb-30">
         <div class="container-fluid p-0 m-0">
     
              <div class="owl-carousel" data-nav-dots="true" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-xx-items="1" data-space="20">
                 <div class="item">
                  <img class="img-fluid full-width" src="public/images/logo1.jpg" alt="">
                </div>
                 <div class="item ">
                  <video src="public/images/video_maquinados.mp4" autoplay muted loop style="max-width:100%!important;max-height:100%!important"></video>
                </div>
                 <div class="item">
                  <img class="img-fluid full-width" src="public/images/nosotros3.jpg" alt="">
                </div>
             </div>
       
   			</div>
       </div>
    </div>
    <!-- ============================================ -->
  
  </div>
  </div>
</section>

<!--seccion mision vision valores--->

<section class="service white-bg page-section-ptb pt-0">
  <div class="container">
    <div class="row">
         
         <div class="col-lg-4 col-md-4 pr-5 text-center ">
         	<img style="width: 15%;" src="public/images/mision.png"  class="pb-3 slideInLeft wow">
            <div class="mb-30">
              <h1 class="text-center zoomIn wow">MISIÓN</h1>
              <label class="text-justify zoomIn wow">Ser una empresa líder y confiable. Estableciendo mejoras continuas en nuestros procesos y servicios superando las expectativas de nuestros clientes.</label>
            </div>
         </div>
         <div class="col-lg-4 col-md-4 pr-5 text-center  zoomIn wow">
         	<img style="width: 15%;" src="public/images/vision.png"  class="pb-3 slideInLeft wow">
            <div class="mb-30">
              <h1 class="text-center zoomIn wow">VISIÓN</h1>
              <label class="text-justify zoomIn wow">Con nuevas tecnologías, personal capacitado y herramientas nos enfocamos a seguir creciendo ofreciendo calidad, precisión y satisfacción a nuestros clientes y colaboradores.</label>
            </div>
         </div>
         <div class="col-lg-4 col-md-4 pr-5 text-center  zoomIn wow">
         	<img style="width: 15%;" src="public/images/valores.png"  class="pb-3 slideInLeft wow">
            <div class="mb-30">
              
              <h1 class="text-center zoomIn wow">VALORES</h1>
              
              <label class="text-justify zoomIn wow">Sembrando y fomentando los valores de la enseñanza, responsabilidad, honestidad y respeto a nuestros clientes y el medio ambiente.</label>
              
            </div>
         </div>
      </div>
   </div>
 </section>

<!-- seccion nuestros clientes-->

<section>
   <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 text-center">
            <h1 class="rubberBand wow">CLIENTES</h1>
  <label>Empresas 100% satisfechas con nuestro trabajo</label>
        </div>
        <div class="col-lg-12 col-md-12 pt-5">
          <div class="clients-list">
             <div class="owl-carousel" data-nav-dots="false" data-items="4" data-md-items="4" data-sm-items="2" data-xs-items="1" data-xx-items="1">
               <div class="item">
                  <img class="img-fluid mx-auto" src="public/images/clientes-06.png" alt="">
               </div>
               <div class="item">
                  <img class="img-fluid mx-auto" src="public/images/clientes-07.png" alt="">
               </div>
               <div class="item">
                  <img class="img-fluid mx-auto" src="public/images/clientes-08.png" alt="">
               </div>
               <div class="item">
                  <img class="img-fluid mx-auto" src="public/images/clientes-09.png" alt="">
               </div>
               <div class="item">
                  <img class="img-fluid mx-auto" src="public/images/clientes-01.png" alt="">
               </div>
               <div class="item">
                  <img class="img-fluid mx-auto" src="public/images/clientes-05.png" alt="">
               </div>
               <div class="item">
                  <img class="img-fluid mx-auto" src="public/images/clientes-03.png" alt="">
               </div>
             </div>
          </div>
        </div>
     </div>
   </div>
 </section>
<!-- fin seccion nuestros clientes-->

<!---seccion de contacto--->
<div class="container text-center pt-5">
	<h1 class="rubberBand wow" style="padding-top:5%">CONTACTANOS</h1>
    <div class="row" style="padding-top:5%;padding-bottom:10%">

      <div class="col-lg-6 sm-mb-30" style="background: url(public/images/imagen_formulario.jpg);background-position: center;background-repeat: no-repeat;background-size: cover;position: relative;">
      </div>

        <div class="col-lg-6">
          <div id="formmessage">Success/Error Message Goes Here</div>
            <form id="contactform" role="form" method="post" action="php/contact-form.php">
              <div class="contact-form form-inline clearfix">
                <div class="section-field">
                  <input id="name" type="text" placeholder="Nombre*" class="form-control" name="name">
                </div>
                <div class="section-field">
                  <input type="email" placeholder="Email*" class="form-control" name="email">
                </div>
                <div class="section-field">
                  <input type="number" placeholder="Teléfono*" class="form-control" name="phone">
                </div>
  			
                <div class="section-field xs-w-100" style="width: 36%">
                  <input type="text" placeholder="Asunto*" class="form-control" name="asunto">
                </div>
                
                <div class="section-field selectformulario1 xs-mb-20">
                  <div class="box">
                    <select class="wide fancyselect" name="producto">
            
                      <option value="SIN SELECCIÓN" disabled="true" selected="true">Seleciona un Proyecto/servicio</option>
                      <option disabled="true">Seleciona un Proyecto</option>
                      <option value="FORMING EN DURO">FORMING EN DURO</option>
                      <option value="LIFTING PLATE">LIFTING PLATE</option>
                      <option value="PIZADOR INFERIOREN DURO">PIZADOR INFERIOREN DURO</option>
                      <option value="53-1_01_001_ZAPATA_INF_810_697_8">53-1_01_001_ZAPATA_INF_810_697_8</option>
                      <option value="MOLDE PARA INYECCION DE PLASTICO">MOLDE PARA INYECCION DE PLASTICO</option>
                      <option value="MOLDE PARA FUNDICION">MOLDE PARA FUNDICION</option>
                      <option value="AUQ5-389445M15000_SGR_HINTERBODEN">AUQ5-389445M15000_SGR_HINTERBODEN</option>
              <option value="VW37-526065M14X00_AH_SGR_MODULTRAEGER">VW37-526065M14X00_AH_SGR_MODULTRAEGER</option>
                      <option value="DC_963 PRENSA _GENERAL_PRESS PART">DC_963 PRENSA _GENERAL_PRESS PART</option>
                      <option value="5Q0 803 113 A_LH-RH">5Q0 803 113 A_LH-RH</option>
                      <option value="BMOK-60000462455TEM1000">BMOK-60000462455TEM1000</option>
                      <option value="VW518-858-965A">VW518-858-965A</option>
                      <option value="BM5U386588LH/386589RH">BM5U386588LH/386589RH</option>
                      <option value="DR77-648164">DR77-648164</option>
                      <option value="613586_AH_T01_CLOSING_PLATE_RLCA">613586_AH_T01_CLOSING_PLATE_RLCA</option>
                      <option value="DC_615296_FORD_CX727">DC_615296_FORD_CX727</option>
                      <option value="615296 RLCA-ASSY">615296 RLCA-ASSY</option>
                      <option value="706317_AD_000_DEFOELEMENT_AUSSEN_LI_VW316_SA">706317_AD_000_DEFOELEMENT_AUSSEN_LI_VW316_SA</option>
                      <option value="VWM-BUMPER_TAREK_706316/318">VWM-BUMPER_TAREK_706316/318</option>
                      <option value="DR77497941">DR77497941</option>
                      <option value="BM5U386608_92347">BM5U386608_92347</option>
                      <option value="DR77-490122">DR77-490122</option>
                      <option value="DR77_60000422137TEM_ZBQUERTRAEGER">DR77_60000422137TEM_ZBQUERTRAEGER</option>
                      <option value="534118_M09_Z01_GEM170-ASSY-TWISTBEAM">534118_M09_Z01_GEM170-ASSY-TWISTBEAM</option>
                      <option value="CHECKING FIXTURE">CHECKING FIXTURE</option>
                      <option value="VW37-526065M14X00_AH_SGR_MODULTRAEGER">VW37-526065M14X00_AH_SGR_MODULTRAEGER</option>

                      <option disabled="true">Seleciona un servicio</option>
                      <option value="CHECKING & HOLDING FIXTURE (HF’S)">CHECKING & HOLDING FIXTURE (HF’S)</option>
                      <option value="RECTIFICADO">RECTIFICADO</option>
                      <option value="CENTRO DE MAQUINADO CNC">CENTRO DE MAQUINADO CNC</option>
                      <option value="MAQUINADOS CONVENCIONALES">MAQUINADOS CONVENCIONALES</option>
        
                    </select>
                  </div>
                </div>

                <div class="section-field textarea">
                  <textarea class="input-message form-control" placeholder="Mensaje*" rows="7" name="message"></textarea>
                </div>
                <!-- Google reCaptch-->
              <div class="g-recaptcha section-field clearfix d-flex justify-content-center" data-sitekey="6LdwyNUUAAAAADhX6v7h8W7nARWWRpAUDbZknT1t"></div>
                <div class="form-control submit-button text-center" style="background-color: transparent;">
                  <input type="hidden" name="action" value="sendEmail">
                  <button id="submit" name="submit" type="submit" value="Send" class="button rounded-pill pt-1 pb-1"><span>ENVIAR</span></button>
                </div>
              </div>
            </form>
            <div id="ajaxloader" style="display:none"><img class="mx-auto mt-30 mb-30 d-block" src="public/images/loader-04.svg" alt=""></div>
        </div>
    </div>
  </div>
@endsection