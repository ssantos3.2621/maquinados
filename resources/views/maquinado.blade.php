@extends('layouts.header')
@section('content')
<!--header fijo--->
<div class="container-fluid p-0 m-0 padingtop">
    <div class="item">
      <img class="img-fluid full-width " src="public/images/PROYECTOS-SLIDER.jpg" alt="">
    </div>
   </div>
<!--fin header fijo--->

<!--seccion contenido Diseño-->
 <div class="container text-center">
  
    <h2 style="padding-top: 7%" class="flipInY wow">MAQUINADO</h2>

<div class="isotope columns-3 popup-gallery" style="position: relative; height: 1572.1px;padding-top:4%">
             
              <div class="grid-item photography branding" style="position: absolute; left: 0px; top: 0px;">
                  <div class="portfolio-item">
                   <img src="public/images/maquinado.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo" href="Maquinado-Forming-En-Duro"> FORMING EN DURO <br> Cliente: GGM PLANTA PUEBLA </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/maquinado.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
               </div>

              <div class="grid-item photography branding" style="position: absolute; left: 344px; top: 0px;">
               <div class="portfolio-item">
                   <img src="public/images/maquinado1.jpg" alt="">
                     <div class="portfolio-overlay">
                       <h4 class="text-white"> <a id="maquinadodispositivo" href="Maquinado-LiftingPlate"> LIFTING PLATE <br> CLIENTE: BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/maquinado1.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
              </div>
              
              <div class="grid-item illustration" style="position: absolute; left: 688px; top: 0px;">
               <div class="portfolio-item">
                   <img src="public/images/maquinado2.jpg" alt="">
                     <div class="portfolio-overlay">
                         <h4 class="text-white"> <a id="maquinadodispositivo" href="Maquinado-PizadorInferiorDuro"> PIZADOR INFERIOREN DURO <br> CLIENTE: GGM </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/maquinado2.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
                </div>
              
             <div class="grid-item illustration" style="position: absolute; left: 0px; top: 262px;">
              <div class="portfolio-item">
                   <img src="public/images/maquinado4.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo4" href="Maquinado-Proyecto"> 53-1 _ 01_001_ZAPATA_INF_810_697_8  <br>    CLIENTE: PRECISION RIDE </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/maquinado4.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
              </div>
              
              <div class="grid-item branding illustration" style="position: absolute; left: 344px; top: 262px;">
                <div class="portfolio-item">
                   <img src="public/images/maquinado5.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo4" href="Maquinado-MoldeParaInyeccionDePlastico"> MOLDE PARA INYECCION DE PLASTICO <br> CLIENTE: BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/maquinado5.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
                </div>
              
              <div class="grid-item branding illustration" style="position: absolute; left: 688px; top: 262px;">
               <div class="portfolio-item">
                   <img src="public/images/maquinado6.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo" href="Maquinado-MoldeParaFundicion"> MOLDE PARA FUNDICION <br> CLIENTE: JESUS MONTAÑO </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/maquinado6.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
              </div>

               <div class="grid-item branding illustration" style="position: absolute; left: 688px; top: 262px;">
               <div class="portfolio-item">
                   <img src="" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo" href="Maquinado-MoldeParaFundicion">  </h4>
                      </div>
                </div>
              </div>

              <div class="grid-item illustration" style="position: absolute; left: 0px; top: 262px;">
                <div class="portfolio-item">
                   <img src="public/images/dc.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo" href="DispositivoDeGrabado"> AUQ5-389445M15000_SGR_HINTERBODEN  <br> Cliente: BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/dc.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                  </div>
              </div>

         </div>

    </div>

    <br><br><br>

<!--fin seccion contenido Diseño-->


<!---formulario de contacto y mapa-->
<!-- <div class="container text-center pt-5">
  <h1 class="rubberBand wow">PIDE TU COTIZACIÓN</h1>
    <div class="row" style="padding-top:5%;padding-bottom:10%">

      <div class="col-lg-6 sm-mb-30" style="background: url(images/imagen_formulario.jpg);background-position: center;background-repeat: no-repeat;background-size: cover;position: relative;">
      </div>

        <div class="col-lg-6">
          <div id="formmessage">Success/Error Message Goes Here</div>
            <form id="contactform" role="form" method="post" action="php/contact-form.php">
              <div class="contact-form form-inline clearfix">
                <div class="section-field">
                  <input id="name" type="text" placeholder="Nombre*" class="form-control" name="name">
                </div>
                <div class="section-field">
                  <input type="email" placeholder="Email*" class="form-control" name="email">
                </div>
                <div class="section-field">
                  <input type="number" placeholder="Teléfono*" class="form-control" name="phone">
                </div>
        
                <div class="section-field" style="width: 36%">
                  <input type="text" placeholder="Asunto*" class="form-control" name="asunto">
                </div>
                <div class="section-field selectformulario">
                  <div class="box">
                    <select class="wide fancyselect" name="producto">
                      <option  disabled="true" selected="true">Maquinado</option>
                      <option value="FORMING EN DURO">FORMING EN DURO</option>
                      <option value="LIFTING PLATE">LIFTING PLATE</option>
                      <option value="PIZADOR INFERIOREN DURO">PIZADOR INFERIOREN DURO</option>
                      <option value="53-1 _ 01_001_ZAPATA_INF_810_697_8">53-1 _ 01_001_ZAPATA_INF_810_697_8</option>
                      <option value="MOLDE PARA INYECCION DE PLASTICO">MOLDE PARA INYECCION DE PLASTICO</option>
                      <option value="MOLDE PARA FUNDICION">MOLDE PARA FUNDICION</option>
                    </select>
                  </div>
                </div>

                <div class="section-field textarea">
                  <textarea class="input-message form-control" placeholder="Mensaje*" rows="7" name="message"></textarea>
                </div>

              <div class="g-recaptcha section-field clearfix" data-sitekey="6LdwyNUUAAAAADhX6v7h8W7nARWWRpAUDbZknT1t"></div>
                <div class="form-control submit-button text-center" style="background-color: transparent;">
                  <input type="hidden" name="action" value="sendEmail">
                  <button id="submit" name="submit" type="submit" value="Send" class="button rounded-pill pt-1 pb-1"><span>ENVIAR</span></button>
                </div>
              </div>
            </form>
            <div id="ajaxloader" style="display:none"><img class="mx-auto mt-30 mb-30 d-block" src="images/loader-04.svg" alt=""></div>
        </div>
    </div>
  </div> -->
@endsection
