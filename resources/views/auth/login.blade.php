<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="HTML5 Template" />
<meta name="description" content="Webster - Responsive Multi-purpose HTML5 Template" />
<meta name="author" content="potenzaglobalsolutions.com" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>Maquinados Industriales Andres</title>

<!-- Favicon -->
<link rel="shortcut icon" href="public/images/icon.png" />

<!-- font -->
<link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,500,500i,600,700,800,900|Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">

<!-- Plugins -->
<link rel="stylesheet" type="text/css" href="public/css/plugins-css.css" />

<!-- Typography -->
<link rel="stylesheet" type="text/css" href="public/css/typography.css" />

<!-- Shortcodes -->
<link rel="stylesheet" type="text/css" href="public/css/shortcodes/shortcodes.css" />

<!-- Style -->
<link rel="stylesheet" type="text/css" href="public/css/style.css" />

<!-- Responsive -->
<link rel="stylesheet" type="text/css" href="public/css/responsive.css" />

</head>

<body>

<div class="wrapper" >

<div class="bg-overlay-black-60 parallax" style="background-image: url(public/images/login.jpg);">

<!--=================================
 preloader -->

<div id="pre-loader">
    <img src="public/images/loader-04.svg" alt="">
</div>

<!--=================================
 preloader -->


<!--=================================
 login-->

<section class="section-transparent page-section-pb" style="height: 100vh!important;display: flex;
   justify-content: center;
   align-items: center;padding-bottom: 0px">
  <div class="container" >
     <div class="row justify-content-center">

       <div class="col-lg-4 col-md-6">
       <div class="logo text-center mb-30 mt-30">
         </div>

        <div class="login-bg clearfix" style="background-color: rgba(255,255,255,0.8);">

           <div class="login-title text-center" style="background:transparent!important;">
                <a href="{{url('/')}}"><img style="height: 80px;"class="logo-small" id="logo_img" src="public/images/logosinfondo.png" alt="logo"> </a>
                 <h2 class="mb-0" style="color:#2e3192!important;">INICIAR SESIÓN</h2>
            </div>

            <form method="POST" action="{{ route('login') }}">
                        @csrf
             <div class="login-form">
               <div class="section-field mb-20">
                 <label class="mb-10" for="name" style="color:#2e3192!important;">Email</label>
                   <input id="email" type="email" style="padding:7px!important" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                </div>
                <div class="section-field mb-20">
                 <label class="mb-10" for="Password" style="color:#2e3192!important;">Contraseña</label>
                   <input id="password" type="password" style="padding:7px!important" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                </div>
                <div class="section-field">
                   <div class="custom-control custom-checkbox mb-30">
                  <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                  <label class="custom-control-label" for="customControlAutosizing">Recuérdame</label>
                </div>
                  </div>
                  <div class="section-field mb-20 d-flex justify-content-center">
                  <button type="submit" class="btn btn-primary" style="border-radius: 1.5rem;width: 40%;padding: 0.05rem!important;">
                        {{ __('Entrar') }}
                  </button>
                </div>
               </div>
           </form>
           </div>
        </div>
      </div>
  </div>
</section>
<!--=================================
 login-->

 </div>
</div>



<div id="back-to-top"><a class="top arrow" href="#top"><i class="fa fa-angle-up"></i> <span>TOP</span></a></div>

<!--=================================
 jquery -->

<!-- jquery -->
<script src="public/js/jquery-3.4.1.min.js"></script>

<!-- plugins-jquery -->
<script src="public/js/plugins-jquery.js"></script>

<!-- plugin_path -->
<script>var plugin_path = 'public/js/';</script>

<!-- custom -->
<script src="public/js/custom.js"></script>



</body>
</html>
