@extends('layouts.header')
@section('content')
<!--header fijo--->
<div class="container-fluid p-0 m-0 padingtop">
    <div class="item">
      <img class="img-fluid full-width " src="public/images/PROYECTOS-SLIDER.jpg" alt="">
    </div>
   </div>
<!--fin header fijo--->

<!--seccion contenido Diseño-->
 <div class="container text-center">
  
    <h2 style="padding-top: 7%" class="flipInY wow">CHECKING FIXTURE</h2>

    <div class="isotope columns-3 popup-gallery" style="position: relative; height: 1572.1px;padding-top:4%">
             
              <div class="grid-item photography branding" style="position: absolute; left: 0px; top: 0px;">
                  <div class="portfolio-item">
                   <img src="public/images/cf.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo" href="PROYECTOS-DC_963_PRENSA_GENERAL_PRESS_PART"> DC_963 PRENSA _GENERAL_PRESS PART  <br> Cliente: BENTELER DE MEXICO </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/cf.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
               </div>

              <!-- <div class="grid-item photography branding" style="position: absolute; left: 344px; top: 0px;">
               <div class="portfolio-item">
                   <img src="public/images/cf1.jpg" alt="">
                     <div class="portfolio-overlay">
                <h4 class="text-white"> <a id="maquinadodispositivo" href="PROYECTOS-BMOK-60000462455TEM1000"> BMOK-60000462455TEM1000 <br> CLIENTE: BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/cf1.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
              </div> -->
              
              <div class="grid-item illustration" style="position: absolute; left: 688px; top: 0px;">
               <div class="portfolio-item">
                   <img src="public/images/cf3-1.jpg" alt="">
                     <div class="portfolio-overlay">
                         <h4 class="text-white"> <a id="maquinadodispositivo" href="PROYECTOS-5Q0 803 113 A_LH-RH"> 5Q0 803 113 A_LH-RH <br> CLIENTE:BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/cf3-1.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
                </div>
              
              <div class="grid-item illustration" style="position: absolute; left: 0px; top: 262px;">
            <div class="portfolio-item">
                   <img src="public/images/cf3.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo4" href="PROYECTOS-BMOK-60000462455TEM1000"> BMOK-60000462455TEM1000  <br>    CLIENTE:BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/cf3.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
              </div>
              
              <div class="grid-item branding illustration" style="position: absolute; left: 344px; top: 262px;">
                <div class="portfolio-item">
                   <img src="public/images/cf4.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo4" href="PROYECTOS-VW518-858-965A"> VW518-858-965A <br> CLIENTE: BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/cf4.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
                </div>
              
              <div class="grid-item branding illustration" style="position: absolute; left: 688px; top: 262px;">
               <div class="portfolio-item">
                   <img src="public/images/cf5.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo" href="PROYECTOS-BM5U386588LH-386589RH"> BM5U386588LH/386589RH <br> CLIENTE:BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/cf5.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
              </div>

            <div class="grid-item illustration" style="position: absolute; left: 0px; top: 262px;">
            <div class="portfolio-item">
                   <img src="public/images/cf6.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo4" href="PROYECTOS-DR77-648164"> DR77-648164  <br>    CLIENTE:BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/cf6.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
              </div>
              
              <!-- <div class="grid-item branding illustration" style="position: absolute; left: 344px; top: 262px;">
                <div class="portfolio-item">
                   <img src="public/images/cf7.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo4" href="PROYECTOS-DR77-648164"> BM5U386588LH/386589RH <br> CLIENTE: BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/cf7.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
                </div> -->
              
              <div class="grid-item branding illustration" style="position: absolute; left: 688px; top: 262px;">
               <div class="portfolio-item">
                   <img src="public/images/cf8.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo" href="PROYECTOS-613586_AH_T01_CLOSING_PLATE_RLCA"> 613586_AH_T01_CLOSING_PLATE_RLCA <br> CLIENTE:BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/cf8.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
              </div>


              <!-- <div class="grid-item illustration" style="position: absolute; left: 0px; top: 262px;">
            <div class="portfolio-item">
                   <img src="public/images/cf9.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo4" href="PROYECTOS-615296 RLCA-ASSY"> 613586_AH_T01_CLOSING_PLATE_RLCA  <br>    CLIENTE:BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/cf9.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
              </div>
               -->
              <div class="grid-item branding illustration" style="position: absolute; left: 344px; top: 262px;">
                <div class="portfolio-item">
                   <img src="public/images/cf10.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo4" href="PROYECTOS-DC_615296_FORD_CX727"> DC_615296_FORD_CX727 <br> CLIENTE: BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/cf10.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
                </div>
              
              <div class="grid-item branding illustration" style="position: absolute; left: 688px; top: 262px;">
               <div class="portfolio-item">
                   <img src="public/images/cf11.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo" href="PROYECTOS-615296 RLCA-ASSY"> 615296 RLCA-ASSY <br> CLIENTE:BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/cf11.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
              </div>

              <div class="grid-item illustration" style="position: absolute; left: 0px; top: 262px;">
            <div class="portfolio-item">
                   <img src="public/images/cf12.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo4" href="PROYECTOS-706317_AD_000_DEFOELEMENT_AUSSEN_LI_VW316_SA"> 706317_AD_000_DEFOELEMENT_AUSSEN_LI_VW316_SA  <br>    CLIENTE:BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/cf12.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
              </div>
              
              <div class="grid-item branding illustration" style="position: absolute; left: 344px; top: 262px;">
                <div class="portfolio-item">
                   <img src="public/images/cf13.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo4" href="PROYECTOS-VWM-BUMPER_TAREK_706316-318"> VWM-BUMPER_TAREK_706316/318 <br> CLIENTE: BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/cf13.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
                </div>
              
              <div class="grid-item branding illustration" style="position: absolute; left: 688px; top: 262px;">
               <div class="portfolio-item">
                   <img src="public/images/cf14.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo" href="PROYECTOS-DR77497941"> DR77497941 <br> CLIENTE:BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/cf14.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
              </div>

              <div class="grid-item illustration" style="position: absolute; left: 0px; top: 262px;">
            <div class="portfolio-item">
                   <img src="public/images/cf15.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo4" href="PROYECTOS-BM5U386608_92347"> BM5U386608_92347  <br>    CLIENTE:BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/cf15.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
              </div>
              
              <div class="grid-item branding illustration" style="position: absolute; left: 344px; top: 262px;">
                <div class="portfolio-item">
                   <img src="public/images/cf16.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo4" href="PROYECTOS-DR77-490122"> DR77-490122 <br> CLIENTE: BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/cf16.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
                </div>
              
              <div class="grid-item branding illustration" style="position: absolute; left: 688px; top: 262px;">
               <div class="portfolio-item">
                   <img src="public/images/cf17.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo" href="PROYECTOS-DR77_60000422137TEM_ZBQUERTRAEGER"> DR77_60000422137TEM_ZBQUERTRAEGER <br> CLIENTE:BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/cf17.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
              </div>

              <div class="grid-item branding illustration" style="position: absolute; left: 344px; top: 262px;">
                <div class="portfolio-item">
                   <img src="public/images/cf18.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo4" href="PROYECTOS-534118_M09_Z01_GEM170-ASSY-TWISTBEAM"> 534118_M09_Z01_GEM170-ASSY-TWISTBEAM <br> CLIENTE: BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/cf18.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
                </div>
              
              <div class="grid-item branding illustration" style="position: absolute; left: 688px; top: 262px;">
               <div class="portfolio-item">
                   <img src="public/images/cf2.jpg" alt="">
                     <div class="portfolio-overlay">
                        <h4 class="text-white"> <a id="maquinadodispositivo" href="PROYECTOS-CHECKING FIXTURE"> CHECKING FIXTURE <br> CLIENTE:BENTELER </a> </h4>
                      </div>
                    <a class="popup portfolio-img" href="public/images/cf2.jpg"><i class="xs-mt-5 fa fa-arrows-alt" style="font-size: 30px;margin-top:10px"></i></a>
                </div>
              </div>

         </div>

    </div>

    <br><br><br>

<!--fin seccion contenido Diseño-->

@endsection
